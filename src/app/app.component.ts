import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiCallsService } from './services/api-calls.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(
    private router: Router,
    public translate: TranslateService,
    private api: ApiCallsService
    ) { 
      this.translate.addLangs(['en', 'ar']);
      this.translate.setDefaultLang('en');
      this.api.langValue.subscribe(val =>{
        this.translate.use('en'); 
      })
    }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
