import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment'

@Injectable({
    providedIn: 'root'
})
export class ApiCallsService {
    //behaviour subject to get the and set the language 
    public langDataSource = new BehaviorSubject('en');
    langValue = this.langDataSource.asObservable();
    
    constructor(
        private http: HttpClient
    ) { }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }
    // Handle API errors
    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };

    // Create a new user
    registerUser(user): Observable<any> {
        return this.http
            .post<any>('/register', user, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    // Login a User
    loginUser(user): Observable<any> {
        return this.http
            .post<any>('/login', user, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
}
