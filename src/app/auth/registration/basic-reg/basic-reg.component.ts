import { Component, OnInit } from '@angular/core';
import { ApiCallsService } from 'src/app/services/api-calls.service';
import { Router } from '@angular/router';
import { ToastData, ToastOptions, ToastyService,ToastyConfig} from 'ng2-toasty';

@Component({
    selector: 'app-basic-reg',
    templateUrl: './basic-reg.component.html',
    styleUrls: ['./basic-reg.component.scss']
})
export class BasicRegComponent implements OnInit {
    username: string;
    email: string;
    password: string;
    confirmPassword: string;
    isAdmin: boolean
    constructor(
        private api: ApiCallsService,
        private router: Router,
        private toastyService: ToastyService,
        private toastyConfig: ToastyConfig
    ) { }

    ngOnInit() {
        document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    }

    signUp() {
        if (this.username && this.email && this.password && this.confirmPassword && this.password == this.confirmPassword) {
            let reqObj = {
                username: this.username,
                email: this.email,
                password: this.password,
                isAdmin: this.isAdmin
            }
            this.api.registerUser(reqObj).subscribe(res => {
                if (res.status == 'success') {
                    this.addToast(res.msg,res.status);
                    this.router.navigate(['auth/login']);
                } else {
                    this.addToast(res.msg,res.status);
                }
            });
        }

    }
    userType(event) {
        this.isAdmin = event.target.value;
    }
    
    addToast(msg, state) {
        var toastOptions: ToastOptions = {
            title: state,
            msg: msg,
            showClose: true,
            timeout: 2000,
            theme: 'default',
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: function (toast: ToastData) {
                console.log('Toast ' + toast.id + ' has been removed!');
            }
        };
        if (state == 'success') {
            this.toastyService.success(toastOptions);
        } else {
            this.toastyService.error(toastOptions);
        }
    }
}
