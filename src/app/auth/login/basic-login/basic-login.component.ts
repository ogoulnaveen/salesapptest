import { Component, OnInit } from '@angular/core';
import { ApiCallsService } from 'src/app/services/api-calls.service';
import { Router, NavigationExtras } from '@angular/router';
import { ToastData, ToastOptions, ToastyService,ToastyConfig} from 'ng2-toasty';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {
  email: string;
  password: string;
  lang: string = 'select';

  constructor(
    private api: ApiCallsService,
    private router: Router,
    private toastyService: ToastyService,
    public translate: TranslateService
    
  ) { }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
  }
  
  login(){
      if(this.email && this.password && this.lang){
        let reqObj = {
          email: this.email,
          password: this.password,
        }
        this.api.loginUser(reqObj).subscribe(res => {
          if (res.status == 'success') {
              this.addToast(res.msg,res.status);
              //pass language value to dashboard component
              let navigationExtras: NavigationExtras = {
                queryParams: {
                    lang: this.lang
                }
            };
              this.router.navigate(['dashboard'],navigationExtras);
          } else {
              this.addToast(res.msg,res.status);
          }
      });
      }
  }

  //change the app language
  onLangSelect(event){
    this.api.langDataSource.next(event.target.value);
    this.translate.use(event.target.value);
  }

  //to show toast/alert on any action
  addToast(msg, state) {
    // Just add default Toast with title only
    //this.toastyService.default('Hi there');
    // Or create the instance of ToastOptions
    var toastOptions:ToastOptions = {
        title: state,
        msg: msg,
        showClose: true,
        timeout: 2000,
        theme: 'default',
        onAdd: (toast:ToastData) => {
            console.log('Toast ' + toast.id + ' has been added!');
        },
        onRemove: function(toast:ToastData) {
            console.log('Toast ' + toast.id + ' has been removed!');
        }
    };
    // Add see all possible types in one shot
    if(state == 'success'){
        this.toastyService.success(toastOptions);
    }else{
        this.toastyService.error(toastOptions);
    }
}
}
